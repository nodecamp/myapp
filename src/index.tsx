import {useQuery} from 'react-query';
import {groupBy, reduce} from 'lodash';
import React, {useEffect, useState} from 'react';
import {ActivityIndicator, FlatList, Text, TextInput, View} from 'react-native';

import {config} from './config';

interface Entry {
  API: string;
  Link: string;
  Category: string;
}

interface DS {
  category: string;
  values: Entry[];
}

interface FnUniqProps {
  arr: Entry[];
}

const preprocess = ({arr}: FnUniqProps) => {
  return reduce(
    groupBy(arr, 'Category'),
    (prev, curr, key) => {
      prev.push({
        category: key,
        values: curr,
      });

      return prev;
    },
    [] as DS[],
  );
};

const getEntries = async () => {
  const res = await fetch(`${config.BASE_URL}/entries`);
  return res.json();
};

export const List = () => {
  const {data, isLoading} = useQuery('entries', getEntries, {
    select: data => {
      return preprocess({arr: data?.entries});
    },
  });

  const [query, setQuery] = useState('');

  const [searchResults, setSearchResults] = useState<any[]>([]);

  console.log(JSON.stringify(data?.length, null, 2));
  useEffect(() => {
    search();
  }, [query]);

  const search = () => {
    console.log({query});

    if (query.length < 0) return;
    const result = data?.filter(
      entry => entry.category.toLowerCase().indexOf(query.toLowerCase()) > -1,
    );

    if (result && result.length > 0) {
      setSearchResults(result);
    } else {
      setSearchResults([]);
    }
  };
  if (isLoading) {
    return <ActivityIndicator size="large" />;
  }

  return (
    <>
      <TextInput value={query} placeholder="Search" onChangeText={setQuery} />
      {query.length > 0 && (
        <FlatList
          data={searchResults}
          renderItem={({item}) => <SearchItem searchEntry={item} />}
        />
      )}

      {query.length === 0 && (
        <FlatList
          data={data}
          renderItem={({item}) => <ListItem entry={item} />}
        />
      )}
    </>
  );
};

interface ListItemProps {
  entry: DS;
}

const ListItem = ({entry}: ListItemProps) => {
  return (
    <View>
      <Text>Category: {entry.category}</Text>
    </View>
  );
};

interface SearchItemProps {
  searchEntry: DS;
}

const SearchItem = ({searchEntry}: SearchItemProps) => {
  console.log(JSON.stringify({count: searchEntry.values.length}, null, 2));

  return (
    <View>
      <Text>Category: {searchEntry.category}</Text>
      <FlatList
        data={searchEntry.values}
        renderItem={({item}) => (
          <View>
            <Text>{item.API}</Text>
            <Text>{item.Link}</Text>
          </View>
        )}
      />
    </View>
  );
};
